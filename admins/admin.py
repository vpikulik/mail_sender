from django.contrib import admin

from models import Admin


class AdminAdmin(admin.ModelAdmin):
    pass


admin.site.register(Admin, AdminAdmin)
