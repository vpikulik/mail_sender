
from celery import task


@task()
def send_mail(mail_to_customer_id):

    from utils.sendwithus_client import Client
    from mails.models import MailToCustomer

    mail_to_customer = MailToCustomer.objects.select_related(
        'mail').get(pk=mail_to_customer_id)
    mail = mail_to_customer.mail

    if mail.status != 'sending':
        return

    if mail_to_customer.status != 'notsent':
        return

    client = Client()
    client.send_mail(mail_to_customer)

    if mail.is_sent():
        mail.status = 'sent'
        mail.save()


@task
def sync_template(template_id):

    from utils.sendwithus_client import Client
    from mails.models import Template

    # TODO: can template does not exist?
    template = Template.objects.get(pk=template_id)

    client = Client()
    client.update_template(template)
