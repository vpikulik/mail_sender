from django.contrib import admin
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404
from django.utils.safestring import mark_safe
from django.shortcuts import render_to_response

from models import Template, Mail, MailToCustomer
from tasks import sync_template
from utils.sendwithus_client import Client


class TemplateAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (
                'name',
                'subject',
                'html',
            )
        }),
        ('Sendwithus', {
            'fields': (
                'template_id',
                'sendwithus_status',
                'sendwithus_error',
            )
        }),
    )
    readonly_fields = ('template_id', 'sendwithus_status', 'sendwithus_error')

    def save_model(self, request, obj, form, change):
        obj.save()

        sync_template.delay(obj.pk)


class MailToCustomerInline(admin.TabularInline):

    model = MailToCustomer

    fields = ('customer', 'email', 'status', 'preview_link', 'log_link')
    readonly_fields = ('status', 'email', 'preview_link', 'log_link')

    def email(self, obj):
        return obj.customer.email

    def preview_link(self, obj):
        if not obj.pk:
            return ''
        url = reverse(
            'admin:mail_to_customer_preview_view',
            kwargs={'mail_to_customer_id': obj.id},
        )
        return mark_safe('<a href="{}" target="_blank">Preview</a>'.format(url))

    def log_link(self, obj):
        if not obj.pk or not obj.log_id:
            return ''
        url = reverse(
            'admin:mail_log_view',
            kwargs={'mail_to_customer_id': obj.id},
        )
        return mark_safe('<a href="{}" target="_blank">Log</a>'.format(url))


class MailAdmin(admin.ModelAdmin):

    list_display = ('id', 'admin', 'status', 'template')
    list_filter = ('status',)

    fieldsets = (
        (None, {
            'fields': (
                ('admin', 'template'),
                'content',
                'status',
            )
        }),
    )
    readonly_fields = ('admin', 'status')
    inlines = (MailToCustomerInline,)
    change_form_template = 'admin/mail_change_form.html'

    def has_change_permission(self, request, obj=None):
        if obj is None:
            return True
        return obj.admin == request.user

    def has_add_permission(self, request):
        return False

    def get_urls(self):
        urls = super(MailAdmin, self).get_urls()
        mail_urls = patterns(
            '',
            url(
                r'^/to_customer/(?P<mail_to_customer_id>[0-9]+)/preview/$',
                self.admin_site.admin_view(self.mail_to_customer_preview_view),
                name='mail_to_customer_preview_view',
            ),
            url(
                r'^/to_customer/(?P<mail_to_customer_id>[0-9]+)/log/$',
                self.admin_site.admin_view(self.mail_log_view),
                name='mail_log_view',
            )
        )
        return mail_urls + urls

    def response_change(self, request, mail):
        response = super(MailAdmin, self).response_change(request, mail)

        if '_send_mail' in request.POST:
            mail.send()

        return response

    def mail_to_customer_preview_view(self, request, mail_to_customer_id):
        try:
            mail_to_customer = MailToCustomer.objects.get(
                pk=mail_to_customer_id)
        except MailToCustomer.DoesNotExist:
            raise Http404()

        client = Client()
        content = client.render_mail(mail_to_customer)

        return HttpResponse(content)

    def mail_log_view(self, request, mail_to_customer_id):
        try:
            mail_to_customer = MailToCustomer.objects.get(
                pk=mail_to_customer_id)
        except MailToCustomer.DoesNotExist:
            raise Http404()

        client = Client()
        log_data = client.get_log(mail_to_customer)
        return render_to_response('mail_log.html', {'logs': log_data})


admin.site.register(Template, TemplateAdmin)
admin.site.register(Mail, MailAdmin)
