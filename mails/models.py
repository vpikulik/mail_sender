from django.conf import settings
from django.db import models

from tinymce.models import HTMLField

from tasks import send_mail


class Template(models.Model):

    STATUSES = (
        ('new', u'New'),
        ('failed', u'Failed'),
        ('updated', u'Updated'),
    )

    template_id = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    html = models.TextField()

    sendwithus_status = models.CharField(
        max_length=10,
        choices=STATUSES,
        default='new'
    )
    sendwithus_error = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name


class Mail(models.Model):

    STATUSES = (
        ('draft', u'Draft'),
        ('sending', u'Sending'),
        ('sent', u'Sent'),
    )

    content = HTMLField(null=True)
    status = models.CharField(max_length=10, choices=STATUSES, default='draft')
    template = models.ForeignKey(Template)
    admin = models.ForeignKey(settings.AUTH_USER_MODEL)
    customers = models.ManyToManyField(
        'customers.Customer',
        through='mails.MailToCustomer'
    )

    def __unicode__(self):
        # TODO: make more readble
        return u'Email {}'.format(self.pk)

    def get_footer(self):
        return self.admin.mail_footer

    def send(self):
        if self.status != 'draft':
            return
        self.status = 'sending'
        self.save()
        mail_to_customer_list = MailToCustomer.objects.filter(mail=self)
        for mail_to_customer in mail_to_customer_list:
            send_mail.delay(mail_to_customer.pk)

    def is_sent(self):
        if self.status == 'sent':
            return True

        if self.status == 'draft':
            return False

        not_sent_count = MailToCustomer.objects.filter(
            mail=self, status='notsent').count()
        return not_sent_count == 0


class MailToCustomer(models.Model):

    STATUSES = (
        ('notsent', u'Was not sent yet'),
        ('failed', u'Failed'),
        ('success', u'Success'),
    )

    mail = models.ForeignKey(Mail)
    customer = models.ForeignKey('customers.Customer')
    status = models.CharField(
        max_length=10,
        choices=STATUSES,
        default='notsent'
    )
    log_id = models.CharField(max_length=255, null=True)
