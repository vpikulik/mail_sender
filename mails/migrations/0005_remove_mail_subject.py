# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mails', '0004_mailtocustomer_log_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mail',
            name='subject',
        ),
    ]
