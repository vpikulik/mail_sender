# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mails', '0003_auto_20150915_1957'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailtocustomer',
            name='log_id',
            field=models.CharField(max_length=255, null=True),
            preserve_default=True,
        ),
    ]
