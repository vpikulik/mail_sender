# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(null=True)),
                ('status', models.CharField(default=b'draft', max_length=10, choices=[(b'draft', 'Draft'), (b'sending', 'Sending'), (b'sent', 'Sent')])),
                ('admin', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MailToCustomer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'notsent', max_length=10, choices=[(b'notsent', 'Was not sent yet'), (b'failed', 'Failed'), (b'success', 'Success')])),
                ('customer', models.ForeignKey(to='customers.Customer')),
                ('mail', models.ForeignKey(to='mails.Mail')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='mail',
            name='customers',
            field=models.ManyToManyField(to='customers.Customer', through='mails.MailToCustomer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mail',
            name='template',
            field=models.ForeignKey(to='mails.Template'),
            preserve_default=True,
        ),
    ]
