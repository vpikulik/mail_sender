# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('mails', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='template',
            name='html',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='template',
            name='sendwithus_error',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='template',
            name='sendwithus_status',
            field=models.CharField(default=b'new', max_length=10, choices=[(b'new', 'New'), (b'failed', 'Failed'), (b'updated', 'Updated')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='template',
            name='subject',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='template',
            name='template_id',
            field=models.CharField(max_length=255, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mail',
            name='content',
            field=tinymce.models.HTMLField(null=True),
            preserve_default=True,
        ),
    ]
