
from mock import patch, call

from django.test import TestCase

from customers.models import Customer
from mails.models import Mail, MailToCustomer, Template
from admins.models import Admin


class TestMail(TestCase):

    def setUp(self):
        self.admin = Admin.objects.create(username='root')
        self.template = Template.objects.create(name='base')
        self.mail = Mail.objects.create(admin=self.admin,
                                        template=self.template)
        self.customer1 = Customer.objects.create(name='User1',
                                                 email='em1@em.de')
        self.customer2 = Customer.objects.create(name='User2',
                                                 email='em2@em.de')
        self.mail_to_customer1 = MailToCustomer.objects.create(
            mail=self.mail, customer=self.customer1)
        self.mail_to_customer2 = MailToCustomer.objects.create(
            mail=self.mail, customer=self.customer2)

    @patch('mails.models.send_mail')
    def test_send(self, send_mail_mock):
        self.mail.send()

        self.assertEqual(self.mail.status, 'sending')
        self.assertEqual(
            send_mail_mock.delay.call_args_list,
            [
                call(self.mail_to_customer1.id),
                call(self.mail_to_customer2.id),
            ]
        )

    @patch('mails.models.send_mail')
    def test_send_sent_mail(self, send_mail_mock):
        self.mail.status = 'sent'
        self.mail.send()

        self.assertFalse(send_mail_mock.called)

    @patch('mails.models.send_mail')
    def test_send_sending_mail(self, send_mail_mock):
        self.mail.status = 'sending'
        self.mail.send()

        self.assertFalse(send_mail_mock.called)

    def test_is_sent_sent_mail(self):
        self.mail.status = 'sent'
        self.assertTrue(self.mail.is_sent())

    def test_is_sent_draft_mail(self):
        self.assertFalse(self.mail.is_sent())

    def test_is_sent_sending_mail(self):
        self.mail.status = 'sending'

        self.assertFalse(self.mail.is_sent())

        self.mail_to_customer1.status = 'success'
        self.mail_to_customer1.save()
        self.mail_to_customer2.status = 'failed'
        self.mail_to_customer2.save()
        self.assertTrue(self.mail.is_sent())
