from django.db import models


class Customer(models.Model):

    GENDER_CHOICES = (
        ('male', u'Male'),
        ('female', u'Female'),
    )

    name = models.CharField(max_length=255)
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES)
    email = models.EmailField(max_length=255)

    def __unicode__(self):
        return self.name

    def get_greeting(self):
        if self.gender == 'male':
            pattern = u'Sehr geehrter Herr {name}'
        else:
            pattern = u'Sehr geehrte Frau {name}'
        return pattern.format(name=self.name)
