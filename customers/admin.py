from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from mails.models import Template

from models import Customer

from mails.models import Mail, MailToCustomer


class MailToCustomerInline(admin.TabularInline):

    model = MailToCustomer

    fields = ('mail', 'status')
    readonly_fields = ('status', 'mail')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def mail(self, obj):
        return unicode(obj.mail)


class CustomerAdmin(admin.ModelAdmin):

    list_display = ('name', 'gender', 'email')
    search_fields = ('name', 'email')

    inlines = (MailToCustomerInline,)

    def _send_mail_action(self, template_id, request, queryset):
        try:
            template = Template.objects.get(pk=template_id)
        except Template.DoesNotExist:
            # TODO: catch error and show nice message
            raise

        mail = Mail.objects.create(
            template=template,
            admin=request.user,
        )

        for customer in queryset:
            MailToCustomer.objects.create(
                mail=mail,
                customer=customer,
            )
        mail_url = reverse(
            "admin:{}_{}_change".format(
                mail._meta.app_label,
                mail._meta.module_name
            ),
            args=(mail.id,)
        )
        return HttpResponseRedirect(mail_url)

    def get_send_mail_action(self, template_id):
        def send_mail_action(self, request, queryset):
            return self._send_mail_action(template_id, request, queryset)
        return send_mail_action

    def get_actions(self, request):
        actions = super(CustomerAdmin, self).get_actions(request)
        templates = Template.objects.all()
        for template in templates:
            action_name = 'sent_mail_{}'.format(template.id)
            action = self.get_send_mail_action(template.id)
            short_description = u'Send with {} template'.format(template.name)
            actions[action_name] = (action, action_name, short_description)
        return actions


admin.site.register(Customer, CustomerAdmin)
