# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('gender', models.CharField(max_length=6, choices=[(b'male', 'Male'), (b'female', 'Female')])),
                ('email', models.EmailField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
