
from unittest import TestCase

from models import Customer


class TestCustomer(TestCase):

    def test_greeting_male(self):
        customer = Customer(name='Customer', gender='male')
        self.assertEqual(
            customer.get_greeting(),
            u'Sehr geehrter Herr Customer'
        )

    def test_greeting_female(self):
        customer = Customer(name='Customerin', gender='female')
        self.assertEqual(
            customer.get_greeting(),
            u'Sehr geehrte Frau Customerin'
        )
