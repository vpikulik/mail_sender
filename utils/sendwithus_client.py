
import sendwithus

from django.conf import settings
from django.http import Http404


class ApiError(Exception):
    pass


class Client(object):

    def __init__(self):
        self.api = sendwithus.api(api_key=settings.SENDWITHUS_API_KEY)

    def _proceed_response(self, resp):
        if resp.status_code != 200:
            raise ApiError('Api error: {}'.format(resp.content))
        return resp.json()

    def get_template_version(self, template):
        assert template.template_id
        resp = self.api.get_template(template.template_id)
        data = self._proceed_response(resp)
        return data['versions'][0]['id']

    def update_template(self, template):
        try:
            if template.template_id:
                version = self.get_template_version(template)
                resp = self.api.update_template_version(
                    template.name,
                    template.subject,
                    template.template_id,
                    version,
                    text='',
                    html=template.html,
                )
                self._proceed_response(resp)
            else:
                resp = self.api.create_template(
                    name=template.name,
                    subject=template.subject,
                    html=template.html,
                    # TODO: generate text from html
                    text='Default text',
                )
                data = self._proceed_response(resp)
                template.template_id = data['id']
        except ApiError as exc:
            template.sendwithus_status = 'failed'
            template.sendwithus_error = str(exc)
        else:
            template.sendwithus_status = 'updated'
            template.sendwithus_error = None
        finally:
            template.save()

    def _get_email_data(self, mail_to_customer):
        mail = mail_to_customer.mail
        customer = mail_to_customer.customer
        return {
            'subject': mail.template.subject,
            'greeting': customer.get_greeting(),
            'body': mail.content,
            'footer': mail.get_footer(),
        }

    def send_mail(self, mail_to_customer):
        resp = self.api.send(
            mail_to_customer.mail.template.template_id,
            mail_to_customer.customer.email,
            self._get_email_data(mail_to_customer),
        )
        try:
            resp_data = self._proceed_response(resp)
        except ApiError:
            mail_to_customer.status = 'failed'
        else:
            mail_to_customer.log_id = resp_data['receipt_id']
            mail_to_customer.status = 'success'
        finally:
            mail_to_customer.save()

    def render_mail(self, mail_to_customer):
        resp = self.api.render(
            mail_to_customer.mail.template.template_id,
            self._get_email_data(mail_to_customer),
        )
        try:
            data = self._proceed_response(resp)
        except ApiError:
            raise Http404()
        else:
            return data['html']

    def get_log(self, mail_to_customer):
        if not mail_to_customer.log_id:
            return
        resp = self.api.get_log(mail_to_customer.log_id)
        try:
            data = self._proceed_response(resp)
        except ApiError:
            raise Http404()
        else:
            return data
